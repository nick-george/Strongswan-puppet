node "vultr.guest" { include strongswan 
  class { 'strongswan::presets::roadwarrior': 
    #auto => 'foo' 
  } 
  strongswan::conn { 'testconn':
	params => {
		'ikelifetime'   => test, 
    		'auto'          => test, 
		'keylife'       => test, 
		'rekeymargin'   => test, 
		'keyingtries'   => test, 
		'keyexchange' 	=> test, 
		'left'		=> test, 
		'leftauth'	=> test, 
		'leftsendcert'	=> test, 
		'leftcert'	=> test, 
		'leftsubnet'    => test, 
		'right'		=> test, 
		'rightsourceip' => test, 
		'rightdns'	=> test, 
    		'dpdaction'     => test, 
		'dpddelay'	=> test, 
	},
	secrets  => { 'RSA'  => 'mykey'}	
  }
strongswan::conn { 'third':
        params => {
                'ikelifetime'   => test,
                'auto'          => test,
                'keylife'       => test,
                'rekeymargin'   => test,
                'keyingtries'   => test,
                'keyexchange'   => test,
                'left'          => test,
                'leftauth'      => test,
                'leftsendcert'  => test,
                'leftcert'      => test,
                'leftsubnet'    => test,
                'right'         => test,
                'rightsourceip' => test,
                'rightdns'      => test,
                'dpdaction'     => test,
                'dpddelay'      => test,
        },
        secrets  => { 'RSA'  => 'mykey'}
  }
}
