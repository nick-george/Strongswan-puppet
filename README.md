# strongswan

#### Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with strongswan](#setup)
    * [What strongswan affects](#what-strongswan-affects)
    * [Setup requirements](#setup-requirements)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)


## Overview

Module for installing and configuring strongswan service. 
[strongswan](https://www.strongswan.org/)
Supporting Centos 7.x 

## Setup

### What strongswan affects

* Installs Strongswan package, assumes this is available in your repo.
* By default will set service to running and enabled.
* Configures iptables to allow ipsec ports and configures forwarding.
* Enables ipv4 forward in sysctl.conf


### Setup Requirements 

1. Required modules
  * thias-sysctl
  * puppetlabs-firewall
  * puppetlabs-stdlib

### Usage

By default calling the class alone will install Strongswan but not configure any connections.

To create a new connection you can pass in key => value pairs. These will be stored in params and secrets and used by conn.pp to create connection.
This will be a seperate ipsec conf file and secrets file for each connection included. And will store them in ipsec.d/connections and ipsec.d/secrets.

```
 strongswan::conn { 'example':
  params => {
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
    'key' => value,
  },

secrets => { 'RSA' => 'mykey'}

  }
```

#### presets 
To configure the road warrior preset. Note by default this will require certificates which can be created with strongswan pki or openssl.
[Cert creation info](https://www.vultr.com/docs/using-strongswan-for-ipsec-vpn-on-centos-7)

```
class { 'strongswan::presets::roadwarrior':
  } 
```
override defaults

```
class { 'strongswan::presets::roadwarrior':
	$servercert = 'foo.pem'
  }
```

## Reference



 `strongswan::config`: configures the folders and files required.  
 `strongswan::conn`: generates ipsec config files based on key value pairs passed in.  
 `strongswan::firewall`: uses the puppetlabs-firewall module to open ipsec ports and allow forwarding.   
 `strongswan::install`: installs strongswan package  
 `strongswan::network`: uses the thias-sysctl module to enable forwarding in the kernel.  
 `strongswan::service`: manages the strongswan service  
 

See [Strongswan wiki](https://wiki.strongswan.org/projects/strongswan/wiki/ConnSection) for more info about connection parameters.

## Limitations

At the moment I have only tested Centos 7. 


