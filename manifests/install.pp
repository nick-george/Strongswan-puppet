# Class Strongswan::install
#
# Installs the Strongswan package
#
# Optional paremeters
#
# = $package =
# The name of the package to be installed
#
# = $version =
# Version of package to install
#
#
#
class strongswan::install{
  package { "$strongswan::package":
    ensure => "$strongswan::version",
  }
}
