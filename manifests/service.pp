# Class Strongswan::service
# Manages the Strongswan service
#
# parameters
#
# = $service =
# The name of the service that shousld be managed
#

class strongswan::service{
  service {"$strongswan::service_name":
  ensure => "$strongswan::service_ensure",
  enable => "$strongswan::service_enable",
  }
}
